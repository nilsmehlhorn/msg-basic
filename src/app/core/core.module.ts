import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ENV, getEnv } from './providers/environment.provider';

@NgModule({
  declarations: [],
  providers: [
    {
      provide: ENV,
      useFactory: getEnv
    }
  ],
  imports: [CommonModule]
})
export class CoreModule {}
