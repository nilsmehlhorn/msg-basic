import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConfirmLeaveGuard implements CanDeactivate<unknown> {
  canDeactivate(): boolean {
    return window.confirm('Do you want to leave?');
  }
}
