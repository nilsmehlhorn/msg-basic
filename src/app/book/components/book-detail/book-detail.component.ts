import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Book } from '../../models/book';
import { BookApiService } from '../../services/book-api.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookDetailComponent implements OnInit {
  book$: Observable<Book> = EMPTY;

  constructor(private route: ActivatedRoute, private books: BookApiService) {}

  ngOnInit(): void {
    this.book$ = this.route.params.pipe(
      switchMap(params => this.books.byIsbn(params.isbn))
    );
  }
}
