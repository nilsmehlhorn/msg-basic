import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Book } from './models/book';
import { BookApiService } from './services/book-api.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent {
  bookSearchTerm = '';
  books$: Observable<Book[]>;

  constructor(private bookApi: BookApiService, private router: Router) {
    this.books$ = this.bookApi.getAll();
  }

  goToBookDetails(book: Book) {
    console.log('Navigate to book details, soon ...');
    this.router.navigate(['books', 'details', book.isbn]);
  }

  updateBookSearchTerm(input: Event) {
    this.bookSearchTerm = (input.target as HTMLInputElement).value;
  }
}
