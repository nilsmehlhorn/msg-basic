import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BookRoutingModule } from './book-routing.module';
import { BookComponent } from './book.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { BookFilterPipe } from './pipes/book-filter/book-filter.pipe';

@NgModule({
  declarations: [
    BookComponent,
    BookCardComponent,
    BookFilterPipe,
    BookDetailComponent
  ],
  imports: [CommonModule, BookRoutingModule],
  exports: [BookComponent]
})
export class BookModule {}
