import { Component, Inject } from '@angular/core';
import { Environment } from '../environments/ienvironment';
import { ENV } from './core/providers/environment.provider';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(@Inject(ENV) private env: Environment) {
    console.log(`Running in production: ${env.production}`);
  }
}
